const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
        console.log(entry)
        if (entry.isIntersecting) {
            entry.target.classList.add('show');
            if(entry.target.getAttribute('id') === 'aboutMe'){
                document.body.style.backgroundColor = '#bcd5ea';
            }
            // else if (entry.target.getAttribute('id') !== 'aboutMe'){
            //     document.body.style.backgroundColor = '#1c3549';
            // }


        }
        if (entry.isIntersecting && entry.target.classList.contains('progress-bar')) {
            const bar = entry.target;
            const ariaValueNow = bar.getAttribute('aria-valuenow');
            bar.style.width = `${ariaValueNow}%`;

        }
    });
});




document.body.style.backgroundColor = '#1e5283';

const hiddenElements = document.querySelectorAll('.hidden, .progress-bar, #aboutMe');
hiddenElements.forEach((el) => observer.observe(el));

